# Fader for Foundry VTT
Adds a simple fading control to playlist sounds. 

Once the module activated, you will notice an `F` control next to the playing button in the playlist directory.
Clicking on `F` while the sound is stopped will play it and fade it in.
Clicking on `F` while the sound is playing will fade it out, the `F` will blink in red until the volume is down.

![screenshot](screenshot.png)

N.B. The track is stopped as soon as the volume is 0 for the person that clicked. If this person has already the volume set to 0 it will not fade at all.