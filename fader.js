Hooks.on("renderPlaylistDirectory", (playlist, html, data) => {
    const controls = html.find(".sound-controls")
    controls.each((k) => {
        // Add fader control
        let fader = document.createElement("a");
        fader.classList.add("sound-control");
        fader.setAttribute("data-action", "sound-fade");
        fader.setAttribute("title", game.i18n.localize("FADER.Fade"));
        fader.innerText = "F";
        controls[k].appendChild(fader);
        fader.addEventListener("click", SoundFade)
    });
});

function SoundFade(event) {
    const playlistId = event.target.parentElement.parentElement.dataset["playlistId"];
    const soundId = event.target.parentElement.parentElement.dataset["soundId"];
    const playlist = game.playlists.get(playlistId);
    const sound = playlist.sounds.filter(i => i._id === soundId)[0].sound;
    const vol = sound.volume * game.settings.get("core", "globalPlaylistVolume");
    if (sound.playing) {
        game.socket.emit("module.fader", { soundId: soundId, playlistId: playlistId, fadeOut: true })
        event.target.classList.add("fadeout");
        sound.fade(0, { duration: 4000 }).then( () => {
            const data = { _id: soundId, playing: false };
            playlist.updateEmbeddedEntity("PlaylistSound", data, {});
        });
    } else {
        const data = { _id: soundId, playing: true };
        playlist.updateEmbeddedEntity("PlaylistSound", data, {}).then( () => {
            game.socket.emit("module.fader", { soundId: soundId, playlistId: playlistId, fadeIn: true })
            sound.fade(vol, { duration: 4000, from: 0 });
        });
    }
}

Hooks.once("init", () => {
    // Listen to the socket
    game.socket.on("module.fader", (data) => {
        const playlist = game.playlists.get(data.playlistId);
        const sound = playlist.sounds.filter(i => i._id === data.soundId)[0].sound;
        const vol = sound.volume * game.settings.get("core", "globalPlaylistVolume");
        if (data.fadeOut) {
            sound.fade(0, { duration: 4000 });
        } else if (data.fadeIn) {
            sound.fade(vol, { duration: 4000, from: 0 });
        }
    });
})
